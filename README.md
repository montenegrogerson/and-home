## Estructura de la aplicación web
La aplicación cuenta con el siguiente árbol de contexto:
![enter image description here](https://i.ibb.co/hgBHTYz/main.png)

Aquí se definen la jerarquía de cada uno de los elementos creados dentro de la aplicación, y cada posición en el árbol tiene un motivo que se detallará a continuación.
### Nivel 1
![enter image description here](https://i.ibb.co/5Yfj0G5/nivel-1.png)

Contiene los elementos más transversales a la aplicación, como los *assets*, y el *environment*.

*Assets*
Esta carpeta almacena gráficos usados por los componentes, y a su vez contiene una carpeta con un par de archivos JSON que contienen textos que sirven como soporte para algunos elementos del header.

*Environment*
El objeto aquí almacenado contiene los endpoints desde donde se cargarán los archivos JSON que alimentarán a los 2 componentes decididos para realizar en esta prueba: *Trámites*, y *Otros temas*.
### Nivel 2
![enter image description here](https://i.ibb.co/gt0xTFZ/nivel-2.png)

La carpeta principal *app* contiene 2 elementos: *modules*, y *components*.

*Modules*
Destinada a guardar los módulos de la aplicación. Para este caso, solo se creó un módulo llamado Home, quien hará las veces de contenedor principal de nuestra aplicación web.

*Components*
Esta carpeta se diseña para almacenar los componentes más trasversales dentro de la aplicación. Son componentes que no guardan una relación directa con aquellos componentes creados dentro de los módulos.

### Nivel 3
![enter image description here](https://i.ibb.co/vYMzz4R/nivel-3.png)

El nivel 3 está compuesto por *home*, y el grupo "*barra-header, header, footer*"

*Home*
Módulo principal de la aplicación, y por ahora, el único. Dentro de este módulo encontramos los componentes que son la base del proyecto.

*barra-header, header, footer*
Este grupo de componentes fueron extraídos del [CDN](https://cdn.www.gov.co/v2/pages/inicio), y corresponden a elementos previamente diseñados y que cumplen una función general en todos los productos de la ADN. No tiene relación directa con los demás componentes.

### Nivel 4
![enter image description here](https://i.ibb.co/Mc7DDm7/nivel-4.png)

Los elementos del nivel 4 pertenecen al módulo Home, y cumplen diferentes tareas:

*Components*
La mayor cantidad de componentes en esta aplicación están almacenados en este punto, y se detallarán en líneas posteriores.

*Interfaces*
Las interfaces son elementos de Typescript que nos facilitan definir las estructuras de datos relevantes que se usan en el proyecto:

 - Otro
![enter image description here](https://i.ibb.co/Q9xfJp0/iOtro.png)

El tipo Otro corresponde a los elementos que se cargarán en la sección *Otros temas*, y cada uno de estos elementos contiene las propiedades descritas en la interfaz.

 - Trámite
![enter image description here](https://i.ibb.co/tm8DWcG/iTramite.png)

El tipo Trámite hace referencia a cada uno de los elementos que se cargan en la sección de *Trámites*, y cuyos datos son usados tanto por la sección del carrusel, como por el listado lateral.

*Services*
La carpeta servicios contiene un archivo configurado para cargar datos de Internet, que en este caso son 2 archivos JSON, uno para alimentar la sección *Otros*, y el otro, para alimentar la sección *Trámites*.
Este servicio es usado en componentes que se detallarán más adelante.

### Nivel 5
![enter image description here](https://i.ibb.co/MRFM7bf/nivel-5.png)

Finalmente se tiene el nivel 5, zona donde están hospedados los componentes que se decidió implementar: *Otros temas*, y *Trámites*, además del componente *home*, quien hará las veces de orquestador de los demás componentes.

Que estén al mismo nivel de hecho no significa que tengan la misma jerarquía, pues lo que se intentó fue aplicar de [estilo 04-06](https://angular.io/guide/styleguide#style-04-06) del sitio oficial de Angular para distribuir los componentes.

## Jerarquía del Home
La jerarquía de los componentes ya puestos en el *home* es la siguiente:
![enter image description here](https://i.ibb.co/YfZmJXR/jerarqui-a-componentes.png)

El home implementa los 2 componentes principales que visualizan los *trámites*, y los *otros temas*.

La implementación de dichos componentes a nivel de código se termina viendo así:

![enter image description here](https://i.ibb.co/MV0shkp/home-html.png)

El componente Trámites a su vez hace algo similar, pues está compuesto por 2 subcomponentes: Carrusel, y Lista, cuya implementación termina siendo como se ve en la siguiente imagen:

![enter image description here](https://i.ibb.co/k5hk0WJ/tramites-html.png)

Para finalizar este componente, la manera como visualizamos esto es así:

![enter image description here](https://i.ibb.co/K7M2K6n/carrusel-vista.png)

Done el carrusel es como tal todo lo que está dentro del borde redondeado azul, y lo señalado por el marco púrpura es el componente ítem que se replica por cada slide del carrusel.

De manera similar se tiene el listado de trámites:

![enter image description here](https://i.ibb.co/Wyz4Z4H/lista-tramites-vista.png)

Aquí, el marco púrpura señala el componente ítem del listado. Este mismo componente es replicado tantas veces el servidor envíe, y cada uno es independiente en cuanto a sus datos.

El componente *Otros* repite la misma lógica, pues se cargan unos datos desde un servidor que son puestos en la pantalla a través de los componente creados:

![enter image description here](https://i.ibb.co/z2cpY6j/otros-vista.png)

Y cada uno de los 4 objetos que vemos aquí es el componente ítem de otros, quien muestra a través de la maqueta creada los datos extraídos del servidor.

## Datos en JSON
Se crearon 2 estructuras JSON, una para cada sección creada.

*Trámites*

![tramites-json.png](https://i.ibb.co/bFvd7Qs/tramites-json.png)

La estructura [JSON para *trámites*](https://api.jsonbin.io/b/5f598dc07243cd7e823955bb) contiene elementos que corresponden a las mismas propiedades definidas en la interfaz Tramite.

*Otros*

![otros-json.png](https://i.ibb.co/5hQyG6T/otros-json.png)

La estructura [JSON para *Otros temas*](https://api.jsonbin.io/b/5f5a4e7cad23b57ef90faa08) tiene las mismas características, pues basta con observar la interfaz Otro para notar que allí se encuentran las mismas propiedades.