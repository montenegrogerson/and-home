import { Component, OnInit } from '@angular/core';
import { DatosService } from './../../modules/home/services/datos.service';

@Component({
  selector: 'app-barra-header',
  templateUrl: './barra-header.component.html',
  styleUrls: ['./barra-header.component.css']
})
export class BarraHeaderComponent implements OnInit {
  public estado: boolean = true;
  public titulo: string;

  constructor(private dataService: DatosService) {
    this.estado = true;
    this.titulo = '';
  }

  ngOnInit(): void {
    this.dataService.getBarraHeader().subscribe( (data:any) =>{
      this.titulo = data.titulo;
    });
  }

  cerrar () {
    this.estado = false;
  }
}
