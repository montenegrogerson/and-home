import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DatosService {

  constructor(private http: HttpClient) { }

  getBuscar(){
    return this.http.get('assets/json/buscar.json');
  }

  getBarraHeader(){
    return this.http.get('assets/json/barraheader.json');
  }

  getTramites(): Observable<any> {
    return this.http.get(environment.tramites,
        {
          headers: new HttpHeaders().set('Content-Type', 'application/json')
        }
    );
  }

  getOtros(): Observable<any> {
    return this.http.get(environment.otros,
        {
          headers: new HttpHeaders().set('Content-Type', 'application/json')
        }
    );
  }
}
