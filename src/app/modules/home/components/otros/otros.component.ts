import { Component, OnInit } from '@angular/core';
import { DatosService } from 'src/app/modules/home/services/datos.service';
import { Otro } from 'src/app/modules/home/interfaces/otro';

@Component({
  selector: 'app-otros',
  templateUrl: './otros.component.html',
  styleUrls: ['./otros.component.css'],
  providers: [DatosService]
})
export class OtrosComponent implements OnInit {
  public otros: Otro[];
  public cargando: boolean;

  constructor(private datosService: DatosService) {
    this.cargando = true;
  }

  ngOnInit(): void {
    this.datosService.getOtros().subscribe(data => {
      this.otros = data;
      this.cargando = false;
    });
  }

}
