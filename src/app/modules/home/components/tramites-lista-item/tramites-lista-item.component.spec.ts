import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TramitesListaItemComponent } from './tramites-lista-item.component';

describe('TramitesListaItemComponent', () => {
  let component: TramitesListaItemComponent;
  let fixture: ComponentFixture<TramitesListaItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TramitesListaItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TramitesListaItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
