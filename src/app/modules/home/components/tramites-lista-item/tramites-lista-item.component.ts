import { Component, OnInit, Input } from '@angular/core';
import { Tramite } from 'src/app/modules/home/interfaces/tramite';

@Component({
  selector: 'app-tramites-lista-item',
  templateUrl: './tramites-lista-item.component.html',
  styleUrls: ['./tramites-lista-item.component.css']
})
export class TramitesListaItemComponent implements OnInit {
  @Input() public tramite: Tramite;

  constructor() { }

  ngOnInit(): void {
  }

  getTramiteDisponible (enLinea): string {
    return enLinea ? "Disponible en línea" : "Trámite presencial";
  }

  getEstiloTramiteDisponible (enLinea) {
    return enLinea ? '#0b457f' : '#3772ff';
  }

  getTramiteCosto (conCosto): string {
    return conCosto ? "Trámite con costo" : "Trámite gratuito";
  }
}
