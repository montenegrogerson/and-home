import { DatosService } from './../../services/datos.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  styleUrls: ['./buscar.component.css']
})
export class BuscarComponent implements OnInit {
  public titulo: string;
  public subtitulo: string;
  public placeholder: string;

  constructor(private dataService: DatosService) {
    this.titulo = '';
    this.subtitulo = '';
    this.placeholder = '';
  }

  ngOnInit(): void {
    this.dataService.getBuscar().subscribe((data: any) => {
      this.titulo = data.titulo;
      this.subtitulo = data.subtitulo;
      this.placeholder = data.input.placeholder;
    })
  }

}
