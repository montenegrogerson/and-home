import { Component, OnInit } from '@angular/core';
import { Tramite } from 'src/app/modules/home/interfaces/tramite';
import { DatosService } from 'src/app/modules/home/services/datos.service';

@Component({
  selector: 'app-tramites',
  templateUrl: './tramites.component.html',
  styleUrls: ['./tramites.component.css'],
  providers: [DatosService]
})

export class TramitesComponent implements OnInit {
  public tramites: Tramite[];

  constructor(private datosService: DatosService) { }

  ngOnInit(): void {
    this.datosService.getTramites().subscribe(data => this.tramites = data);
  }

}
