import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TramitesCarruselComponent } from './tramites-carrusel.component';

describe('TramitesCarruselComponent', () => {
  let component: TramitesCarruselComponent;
  let fixture: ComponentFixture<TramitesCarruselComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TramitesCarruselComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TramitesCarruselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
