import { Component, OnInit, Input } from '@angular/core';
import { Tramite } from 'src/app/modules/home/interfaces/tramite'

@Component({
  selector: 'app-tramites-carrusel',
  templateUrl: './tramites-carrusel.component.html',
  styleUrls: ['./tramites-carrusel.component.css']
})
export class TramitesCarruselComponent implements OnInit {
  @Input() public tramites: Array<Tramite>;
  public indicadores;
  public cargando: boolean;

  constructor() {
    this.cargando = true;
  }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    if (this.tramites) {
      this.indicadores = new Array(this.tramites.length);
      this.cargando = false;
    }
  }

  mostrarCarrusel () {
    if (this.tramites) {
      return true;
    }
  }
}
