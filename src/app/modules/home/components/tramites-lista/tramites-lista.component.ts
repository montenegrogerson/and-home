import { Component, OnInit, Input } from '@angular/core';
import { Tramite } from 'src/app/modules/home/interfaces/tramite'

@Component({
  selector: 'app-tramites-lista',
  templateUrl: './tramites-lista.component.html',
  styleUrls: ['./tramites-lista.component.css']
})
export class TramitesListaComponent implements OnInit {
  @Input() public tramites: Array<Tramite>;

  public cargandoDatos: boolean;

  constructor() {
    this.cargandoDatos = true;
  }

  ngOnInit(): void {
  }

  ngOnChanges () {
    if (this.tramites) {
      this.cargandoDatos = false;
    }
  }

}
