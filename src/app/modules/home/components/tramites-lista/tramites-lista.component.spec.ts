import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TramitesListaComponent } from './tramites-lista.component';

describe('TramitesListaComponent', () => {
  let component: TramitesListaComponent;
  let fixture: ComponentFixture<TramitesListaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TramitesListaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TramitesListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
