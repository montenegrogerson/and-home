import { Component, OnInit, Input } from '@angular/core';
import { Tramite } from 'src/app/modules/home/interfaces/tramite';

@Component({
  selector: 'app-tramites-carrusel-item',
  templateUrl: './tramites-carrusel-item.component.html',
  styleUrls: ['./tramites-carrusel-item.component.css']
})

export class TramitesCarruselItemComponent implements OnInit {
  @Input() public tramite: Tramite;
  private maxLetras: number;

  constructor() {
    this.maxLetras = 63;
  }

  ngOnInit(): void {
  }

  getTituloTruncado (titulo: string): string {
    return titulo.length > this.maxLetras ? titulo.substr(0, this.maxLetras) + '...' : titulo;
  }

  getTramiteDisponible (enLinea): string {
    return enLinea ? "Disponible en línea" : "Trámite presencial";
  }

  getEstiloTramiteDisponible (enLinea) {
    return enLinea ? '#0b457f' : '#3772ff';
  }

  getTramiteCosto (conCosto): string {
    return conCosto ? "Trámite con costo" : "Trámite gratuito";
  }
}
