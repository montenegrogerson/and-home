import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TramitesCarruselItemComponent } from './tramites-carrusel-item.component';

describe('TramitesCarruselItemComponent', () => {
  let component: TramitesCarruselItemComponent;
  let fixture: ComponentFixture<TramitesCarruselItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TramitesCarruselItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TramitesCarruselItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
