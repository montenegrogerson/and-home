import { Component, OnInit, Input } from '@angular/core';
import { Otro } from '../../interfaces/otro';

@Component({
  selector: 'app-otros-item',
  templateUrl: './otros-item.component.html',
  styleUrls: ['./otros-item.component.css']
})
export class OtrosItemComponent implements OnInit {
  @Input() public otro: Otro;
  public imagen: string;

  constructor() {
    this.imagen = "assets/49.png";
  }

  ngOnInit(): void {
  }

  ngOnChanges () {
    if (this.otro) {
      this.imagen = this.otro.img;
    }
  }

}
