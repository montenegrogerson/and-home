import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtrosItemComponent } from './otros-item.component';

describe('OtrosItemComponent', () => {
  let component: OtrosItemComponent;
  let fixture: ComponentFixture<OtrosItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtrosItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtrosItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
