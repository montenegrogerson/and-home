export interface Otro {
    img: string;
    titulo: string;
    descripcion: string;
    url: string;
    textoUrl: string;
}
