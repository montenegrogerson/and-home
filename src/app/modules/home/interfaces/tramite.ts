export interface Tramite {
    titulo: string;
    dependencia: string;
    enLinea: boolean;
    conCosto: boolean;
    url: string;
}
