import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';

import { HomeRoutingModule } from './home-routing.module';
import { BuscarComponent } from './components/buscar/buscar.component';
import { TramitesComponent } from './components/tramites/tramites.component';
import { TramitesCarruselComponent } from './components/tramites-carrusel/tramites-carrusel.component';
import { TramitesCarruselItemComponent } from './components/tramites-carrusel-item/tramites-carrusel-item.component';
import { TramitesListaComponent } from './components/tramites-lista/tramites-lista.component';
import { TramitesListaItemComponent } from './components/tramites-lista-item/tramites-lista-item.component';
import { OtrosComponent } from './components/otros/otros.component';
import { OtrosItemComponent } from './components/otros-item/otros-item.component';

@NgModule({
  declarations: [HomeComponent, BuscarComponent, TramitesComponent, TramitesCarruselComponent, TramitesCarruselItemComponent, TramitesListaComponent, TramitesListaItemComponent, OtrosComponent, OtrosItemComponent],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
